###Data

* Basics of normalization
* Basics of T-SQL(quieries only)
* EF mappings
* EF validations
* EF migrations
* Basics of LINQ to Entities

###Web API

* Basics of HTTP
* Basics of REST
* OWIN, Katana
* Web API 2
* Authentication using ASP.NET Identity
* SignalR
* OData
* Web API Integration testing

###Client Side Web

* Basics of functional programming using underscore
* Basics of asynchronous programming with promises and generators using $q
* Basics of reactive programming using RxJS
* jquery - DOM, events, ajax, CSS Selectors
* templating using handlbars
* HTML5 - websockets, Local Storage, IndexedDB, App Cache, History API
* Basics of Data visualization using D3
* Basics Of Bootstrap(CSS)

###Ember

* Ember routing
* Ember views, controllers, templates
* Ember components
* Form Validations
* Basics of Ember Data
* Ember integration testing

###Misceleanous

* Basics of F#
* Basics of coffeescript
* .NET unit testing using fsunit, fscheck
* Client side unit testing using qunit/karma
* .NET build automation using FAKE
* Client side build automation with grunt or broccoli
* Black box testing using WebDriverJS/canopy
* Error handling, logging and tracing
* Continuous integration

